# README #

It is used to scrape nobroker.com all properties data in bangalore using scrapy freamework and upload the data file to google sheets using google sheets api.
open https://docs.google.com/spreadsheets/d/1ZJr6qtvUEiCbBIKQUKoPWF1pcsLRoAQ3tnwhnrRFjqo/edit#gid=1887194793 in browser to view the data.


Steps need to be followed

	1. create a virtual environment.
	2. type command 'pip install Scrapy'(installs scrapy).
	3. type command 'pip install gspread oauth2client'(installs gspread and oauthclient).
	4. clone the repo.
	5. run the command 'scrapy crawl blackspider -o /tmp/no_boker_bangalore.csv' to make it function correctly.
	6. open link 'https://docs.google.com/spreadsheets/d/1ZJr6qtvUEiCbBIKQUKoPWF1pcsLRoAQ3tnwhnrRFjqo/edit#gid=1887194793' to view the data.
	