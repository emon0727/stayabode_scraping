import scrapy
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import re
import os



class QuotesSpider(scrapy.Spider):
    name = "blackspider"
    start_urls = []
    for i in range(1,10000):
        start_urls.append('https://www.nobroker.in/property/ajax/rent/bangalore/Bengaluru/?nbPlace=ChIJbU60yXAWrjsR4E9-UejD3_g&&lat_lng=12.9715987,77.59456269999998&sharedAccomodation=0&orderBy=nbRank,desc&radius=2&propertyType=rent&pageNo='+str(i))

    def __init__(self, *a, **kw):
        """Attach a callback to the spider_closed signal"""
        super(QuotesSpider, self).__init__(*a, **kw)
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        dispatcher.connect(self.spider_opened, signals.spider_opened)


    def parse(self, response):

        for href in response.css('div.card a::attr(href)'):
            yield response.follow(href, self.parse_each_property)


    def parse_each_property(self,response):

        location = response.css("ul.breadcrumb li a span::text")[2].extract().strip()
        rent = response.css("div.col-xs-3.col-sm-3.solid-border-right h1.detail-title::text")[1].extract().strip().replace("/M","")
        lat = response.css("div.map-details div.row.margin-top-12 div.col-xs-12.col-sm-12::attr(data-latitude)").extract_first().strip().replace(".","")
        long = response.css("div.map-details div.row.margin-top-12 div.col-xs-12.col-sm-12::attr(data-longitude)").extract_first().strip().replace(".","")
        id2 = lat + long

        yield {
            'Title': response.css("h1.detail-title-main::attr(title)").extract_first(),
            'Detail': response.css("h5.margin-top-bottom-0::attr(title)").extract_first(),
            'Area': response.css("div.col-xs-2.col-sm-2.solid-border-right h1.detail-title::text").extract_first().strip(),
            'Deposit':response.css("div.col-xs-2.col-sm-2.solid-border-right h1.detail-title::text")[1].extract().strip(),
            'Rent':rent,
            'Negotiable/Non-negotiable':response.css("div.col-xs-3.col-sm-3.solid-border-right h5::text").extract_first().strip(),
            'Age':response.css("div.propertyAge.col-sm-6.col-xs-6.text-align-left h4::text").extract_first().strip(),
            'Furnishing Status':response.css("div.col-sm-5.col-xs-4 h5::text").extract_first().strip(),
            'Preferred Tenants':response.css("div.tenantType.col-sm-6.col-xs-6.solid-border-right.text-align-left h4::text").extract_first().strip(),
            'Possesion':response.css("div.availableFrom.col-sm-6.col-xs-6.text-align-left h4::text").extract_first().strip(),
            'Bedroom': response.css('h4.detail-title-overview::text').extract_first().strip(),
            'Parking': response.css('div.parkingType.col-sm-6.col-xs-6.solid-border-right.text-align-left h4::text').extract_first().strip(),
            'Balcony': response.css('div.balconyCount.col-sm-6.col-xs-6.solid-border-right.text-align-left h4::text').extract_first().strip(),
            'Water Supply': response.css('h5.semi-bold::text')[3].extract().strip(),
            'Bathroom': response.css('h5.semi-bold::text')[5].extract().strip(),
            'Location':location,
            'longitude':response.css("div.map-details div.row.margin-top-12 div.col-xs-12.col-sm-12::attr(data-longitude)").extract_first().strip(),
            'latitude':response.css("div.map-details div.row.margin-top-12 div.col-xs-12.col-sm-12::attr(data-latitude)").extract_first().strip(),
            'id':id2,
        }


    def spider_closed(self,spider):
        scope = ['https://spreadsheets.google.com/feeds',
                 'https://www.googleapis.com/auth/drive']

        creds = ServiceAccountCredentials.from_json_keyfile_name('ScrapingData-8413727f3e87.json', scope)
        client = gspread.authorize(creds)

        with open("/tmp/no_boker_bangalore.csv", "rb") as csv_file:

            csv_file_data = csv_file.read()
            csv_file_data_encoded = re.sub(r'[^\x00-\x7F]+', '', csv_file_data)

            client.import_csv('1ZJr6qtvUEiCbBIKQUKoPWF1pcsLRoAQ3tnwhnrRFjqo', csv_file_data_encoded)


    def spider_opened(self,spider):
        try:
            os.remove("/tmp/no_boker_bangalore.csv")
        except:
            pass


